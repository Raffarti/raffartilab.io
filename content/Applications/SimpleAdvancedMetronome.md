+++
showonlyimage = false
draft = false
image = "img/sam.png"
date = "2020-12-13"
title = "SAM - Simple Advanced Metronome"
weight = 1
categories = ["Music"]
tags = ["music", "music tool", "c++", "WASM", "android", "linux", "windows"]
+++

A simple yet accurate metronome for musicians.

Available for:
[[Browser]](https://raffarti.gitlab.io/simpleadvancedmetronome/SimpleAdvancedMetronome.html)
[[Linux]](https://gitlab.com/Raffarti/simpleadvancedmetronome/-/jobs/artifacts/master/download?job=Linux_x64)
[[<nobr>Android (F-Droid)</nobr>]](https://f-droid.org/it/packages/raffarti.simpleadvancedmetronome/)
[[<nobr>Android (APK)</nobr>]](https://gitlab.com/Raffarti/simpleadvancedmetronome/-/jobs/artifacts/master/raw/SAM.apk?job=apk)
[[Windows]](https://gitlab.com/Raffarti/simpleadvancedmetronome/-/jobs/artifacts/master/download?job=Win32)

<!--more-->

[<div style="text-align: right">Gitlab</div>](https://gitlab.com/raffarti/simpleadvancedmetronome)

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/sam.png" >}}
{{< /gallery >}}{{< load-photoswipe >}}
